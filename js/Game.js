function init() {
	document.getElementById('start')
			.addEventListener('click', function() {
				initGame();
			}, false);
			
	document.getElementById('restart')
			.addEventListener('click', function() {
				if(gameover) {
					initGame();
				}
			}, false);
			
	
	document.addEventListener("keydown", function(e) {
		//wasd = 87, 65, 83, 68
		switch(e.keyCode) {
			case 87:
				wasd[0] = true;
				break;
			case 83:
				wasd[2] = true;
				break;
			case 65:
				wasd[1] = true;
				player.isLeft = true;
				break;
			case 68:
				wasd[3] = true;
				player.isLeft = false;
				break;
			case 219:	// [
				player.sizeX -= 2;
				player.sizeY = Math.floor(player.sizeX / 2);
				break;
			case 221:	// ]
				player.sizeX += 2;
				player.sizeY = Math.floor(player.sizeX / 2);
				break;
			case 80: // P - pause
				paused = !paused;
				// if unpaused, restart main()
				if(!paused) {
					main();
				}else {
					renderPauseBox();
				}
				break;
			default:
				//console.log(e.keyCode);
				break;
		}
	}, false);
	
	document.addEventListener("keyup", function(e) {
		//wasd = 87, 65, 83, 68
		switch(e.keyCode) {
			case 87:
				wasd[0] = false;
				break;
			case 83:
				wasd[2] = false;
				break;
			case 65:
				wasd[1] = false;
				break;
			case 68:
				wasd[3] = false;
				break;
			default:
				break;
		}
	}, false);
	
	/* LISTENERS */
}//init

function initGame() {
	gamestarted = true;
	gameover = false;
	paused = false;
	
	// init canvas and context
	canvas = document.getElementById('stage');
	// check if canvas is supported
	if(!canvas.getContext) {
		alert("Your browser does not support HTML5 Canvas :(");
		return;
	}
	ctx = canvas.getContext('2d');
	
	// hide splash
	document.getElementById("splash").className = "nodisplay";
	document.getElementById("restart").className = "nodisplay";
	
	//goFullScreen(canvas);
	refreshCanvasSize();
	
	// set canvas background
	canvas.background = new Image();
	canvas.background.src = 'img/background.png';
	
	// reset arrays
	enemies = [];
	
	// reset player
	player.x = canvas.width/2 - (player.sizeX / 2);
	player.y = canvas.height/2;
	player.sizeX = 16
	player.sizeY = 8
	player.veloX = 0;
	player.veloY = 0;
	player.isLeft = false;
	wasd = [false, false, false, false];
	
	
	//	init first 10 enemies
	for(var i = 0; i < 10; i++) {
		addEnemy();
	}
	
	// first fish game
	firstFishMode = document.getElementById('firstFishMode').checked;
	if(firstFishMode) {
		if(firstFishWin) {
			firstFishTries = 0;
		}
		firstFishWin = false;
		stoppedEnemies = 0;
		firstFishTries++;
	}
	
	
	// set fps
	fpsInterval = 1000 / fps;
	then = Date.now();
	startTime = then;
	
	main();
}

function main() {
	now = Date.now();
	elapsed = now - then;
	
	if(elapsed > fpsInterval) {
		then = now - (elapsed % fpsInterval);
	
		if(gameover) {
			if(firstFishMode) {
				if(firstFishWin) {
					renderFishEatWin();
				}else {
					renderFishEatLose();
				}
			}else {
				renderGameOver();
			}
		}else if(!paused) {
			collision();
			draw();
			//levels();
		}
	}// elapsed
	
	//	consistent framerate
	requestAnimationFrame(main);
}

//	reset enemy for immediate use
function addEnemy(index) {
	
	// base scales off of player size
	enemyVars.minX = (player.sizeY / 2);
	enemyVars.maxX = player.sizeX + 50;
	
	enemy.sizeX = Math.floor((Math.random() * enemyVars.maxX) + enemyVars.minX);
	enemy.sizeY = Math.floor(enemy.sizeX/2);
	enemy.y = Math.floor((Math.random() * canvas.height) - (enemy.sizeY / 2));
	enemy.speed = Math.floor((Math.random() * Math.floor(enemyVars.maxSpeed - (enemy.sizeX/100))) + enemyVars.minSpeed);
	if(Math.abs(enemy.speed) > 5) {
		enemy.type = 1;
	}else {
		enemy.type = 0;
	}
	
	
	// determine direction
	if(Math.floor(Math.random() * 2)) {
		enemy.x = enemy.sizeX * -1;
	}else {
		enemy.x = enemy.sizeX + canvas.width;
		enemy.speed *= -1;
	}
	
	// set spawn delay
	enemy.delay = Math.floor(Math.random() * enemyVars.delay);
	
	// TODO: add fish type

	if(typeof index !== 'undefined') {
		enemies[index].x = enemy.x;
		enemies[index].y = enemy.y;
		enemies[index].sizeX = enemy.sizeX;
		enemies[index].sizeY = enemy.sizeY;
		enemies[index].speed = enemy.speed;
		enemies[index].type = enemy.type;
	}else {
		enemies.push(clone(enemy));
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// movement and collision
function collision() {
	// movement velocity
	player.x += player.veloX;
	player.y += player.veloY;
	
	if(wasd[3]) {
		player.veloX += player.veloInc;
	}
	if(wasd[1]) {
		player.veloX -= player.veloInc;
	}
	if(wasd[2] && player.y + player.sizeY < canvas.height) {
		player.veloY += player.veloInc;
	}
	if(wasd[0] && player.y > 0) {
		player.veloY -= player.veloInc;
	}
	
	// screen wrap around
	// right
	if(player.veloX > 0 && player.x > canvas.width) {
		player.x = player.sizeX * -1;
	// left
	}else if(player.veloX < 0 && player.x + player.sizeX < 0) {
		player.x = canvas.width;
	}
	
	// if no vertical movement
	if(!wasd[0] && !wasd[2]) {
		// apply negative velocity
		if(player.veloY < 0) {
			player.veloY += player.veloDec;
		}else if(player.veloY > 0) {
			player.veloY -= player.veloDec;
		}
		// if low velocity, reset to 0
		if(Math.abs(player.veloY) < player.veloDec) {
			player.veloY = 0;
		}
	}
	
	// if no horizontal movement
	if(!wasd[1] && !wasd[3]) {
		// apply negative velocity
		if(player.veloX < 0) {
			player.veloX += player.veloDec;
		}else if (player.veloX > 0) {
			player.veloX -= player.veloDec;
		}
		// if low velocity, reset to 0
		if(Math.abs(player.veloX) < player.veloDec) {
			player.veloX = 0;
		}
	}
	
	// if not moving
	if(!wasd[0] && !wasd[1] && !wasd[2] && !wasd[3]) {
		if(player.y < canvas.height - (player.sizeY / 2)) {
			// invoke sinking ticks
			player.sinkTick--;
			if(player.sinkTick < 0) {
				player.sinkTick = player.sinkTickMax;
				player.veloY += player.veloDec;
			}
		}
	}
	
	// cap max velocity
	if(player.veloX > player.veloMax) {
		player.veloX = player.veloMax
	}else if(player.veloX < player.veloMax * -1) {
		player.veloX = player.veloMax * -1;
	}
	if(player.veloY > player.veloMax) {
		player.veloY = player.veloMax;
	}else if(player.veloY < player.veloMax * -1) {
		player.veloY = player.veloMax * -1;
	}
	
	
	// handle Y offscreen
	if(player.y < 0) {
		player.veloY = Math.abs(player.veloY) / 2;
		player.y = 0;
	}else if (player.y + (player.sizeY) > canvas.height) {
		player.veloY = (Math.abs(player.veloY) / 2) * -1;
		player.y = canvas.height - (player.sizeY);
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	for(var e = 0; e < enemies.length; e++) {
		var ce = enemies[e];
		if(ce.delay > 0) {
			ce.delay--;
		}else {
			// move enemt
			ce.x += ce.speed;
			// if OOB
			if((ce.speed > 0 && ce.x - ce.sizeX > canvas.width) ||
			   (ce.speed < 0 && ce.x + ce.sizeX < 0)) {
				   if(firstFishMode) {
					   enemies[e].speed = 0;
					   stoppedEnemies++;
				   }else {
					   addEnemy(e);
				   }
			}
			// if collision with enemy
			if(intersectPlayer(ce)) {
				if(firstFishMode) {
					if(e === 0) {
						// winning condition
						gameover = true;
						firstFishWin = true;
					}else {
						gameover = true;
					}
				}else {
				   // player is larger, consume enemy
				   if(player.sizeX > ce.sizeX) {
						if(player.sizeX - ce.sizeX < 5) {
							player.sizeX += 2;
						}else {
							player.sizeX++;
						}
					   player.sizeY = Math.floor(player.sizeX / 2);
					   addEnemy(e);
				   // enemy is larger, game over
				   }else {
					   player.eatenBy = ce.sizeX;
					   gameover = true;
				   }
				}
			}
		}
	}
	// all enemies stopped
	if(firstFishMode && stoppedEnemies === 10) {
		gameover = true;
	}
}



// -------------------------------------
function draw() {
	// draw background
	//ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(canvas.background, 0, 0, canvas.width, canvas.height);
	
	// draw player
	if(player.isLeft) {
		ctx.drawImage(fishImgs[0].left, player.x, player.y, player.sizeX, player.sizeY);
	}else {
		ctx.drawImage(fishImgs[0].right, player.x, player.y, player.sizeX, player.sizeY);
		
	}
	
	// draw enemies
	for(var e = 0; e < enemies.length; e++) {
		var ce = enemies[e];
		// left
		if(ce.speed < 0) {
			ctx.drawImage(fishImgs[ce.type].left, ce.x, ce.y, ce.sizeX, ce.sizeY);
		// right
		}else {
			ctx.drawImage(fishImgs[ce.type].right, ce.x, ce.y, ce.sizeX, ce.sizeY);
		}
	}
	
	if(scoreFont != ctx.font) {
		ctx.font = "24px Verdana";
		ctx.textAlign = 'left';
	}
	ctx.strokeText('Size: ' + player.sizeX, 10, 30);
}
function renderPauseBox() {
	ctx.fillStyle = 'black';
	ctx.fillRect(canvas.width/4, canvas.height/4, canvas.width/2, canvas.height/2);
	ctx.stroke();
	
	ctx.font = "48px Verdana";
	ctx.textAlign = 'center';
	ctx.fillStyle = 'white';
	ctx.fillText('PAUSED', canvas.width/2, canvas.height/2);
	ctx.font = "12px Verdana";
	ctx.fillText('Press P to resume.', canvas.width/2, canvas.height/2 + 48);
}
function renderGameOver() {
	ctx.fillStyle = 'aqua';
	ctx.fillRect(canvas.width/4, canvas.height/4, canvas.width/2, canvas.height/2);
	ctx.stroke();
	
	ctx.font = "48px Verdana";
	ctx.textAlign = 'center';
	ctx.fillStyle = 'black';
	ctx.fillText('You got eaten!', canvas.width/2, canvas.height/2);
	ctx.font = "12px Verdana";
	ctx.fillText('Your size: ' + player.sizeX, canvas.width/2, canvas.height/2 + 35);
	ctx.fillText('Eaten by: ' + player.eatenBy + (player.sizeX == player.eatenBy ? ' :^)' : ''), canvas.width/2, canvas.height/2 + 55);
	
	var restart = document.getElementById('restart');
	restart.className = '';
	restart.style = 'margin-left: -' + restart.clientWidth/2 + 'px;';
	
	document.getElementById('stage').className = 'gameover';
}
function renderFishEatWin() {
	ctx.fillStyle = 'aqua';
	ctx.fillRect(canvas.width/4, canvas.height/4, canvas.width/2, canvas.height/2);
	ctx.stroke();
	
	ctx.font = "48px Verdana";
	ctx.textAlign = 'center';
	ctx.fillStyle = 'black';
	ctx.fillText('You ate the first fish!', canvas.width/2, canvas.height/2);
	ctx.font = "12px Verdana";
	ctx.fillText('It took you ' + firstFishTries + ' times!', canvas.width/2, canvas.height/2 + 35);
	
	var restart = document.getElementById('restart');
	restart.className = '';
	restart.style = 'margin-left: -' + restart.clientWidth/2 + 'px;';
	
	document.getElementById('stage').className = 'gameover';
}
function renderFishEatLose() {
	ctx.fillStyle = 'red';
	ctx.fillRect(canvas.width/4, canvas.height/4, canvas.width/2, canvas.height/2);
	ctx.stroke();
	
	ctx.font = "40px Verdana";
	ctx.textAlign = 'center';
	ctx.fillStyle = 'white';
	ctx.fillText('You didn\'t eat the first fish :(', canvas.width/2, canvas.height/2);
	ctx.font = "12px Verdana";
	
	var restart = document.getElementById('restart');
	restart.className = '';
	restart.style = 'margin-left: -' + restart.clientWidth/2 + 'px;';
	
	document.getElementById('stage').className = 'gameover';
}

function levels() {
	// more than 50 uses too much CPU - find a way around this?
	if(enemies.length < 500) {
		if(!hard && player.points % 10 == 0) {
			addEnemy();
			player.points++;
		}else if(hard && player.points % 5 == 0) {
			addEnemy();
			player.points++;
		}
	}
}