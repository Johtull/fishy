var wasd = [false, false, false, false];

var player = {
	x: 64,
	y: 64,
	sizeX: 16,
	sizeY: 8,
	veloX: 0,
	veloY: 0,
	veloMax: 10,
	veloInc: 0.5,
	veloDec: 0.2,
	sinkTick: 0,
	sinkTickMax: 10,
	isLeft: false,
	img_right: 0,
	img_left: 0,
	eatenBy: 0
};

var fishImgs = [
	{
		right: document.getElementById("fish1_right"),
		left: document.getElementById("fish1_left"),
	},
	{
		right: document.getElementById("fish2_right"),
		left: document.getElementById("fish2_left"),
	}
]


var enemyVars = {
	minX: 5,
	maxX: 100,
	minSpeed: 1,
	maxSpeed: 10,
	delay: 50,
	type: 0
};

var enemy = {
	x: 0,
	y: 0,
	sizeX: 16,
	sizeY: 8,
	speed: 0,	// movement direction X
	delay: 0,	// spawn delay
	type: 'man',
	img: 'man'
};

/* GLOBALS */
var canvas = {};
var ctx = {};

var soundEnabled = true;
var gamestarted = false;
var paused = false;
var gameover = false;


var enemies = [];

var ding = new Audio('sounds/ding.wav');
var bird = new Audio('sounds/bird.wav');
var pain = new Audio('sounds/pain.wav');
var boom = new Audio('sounds/boom1.wav');

var scoreFont = "24px Verdana";

// regulate fps
var fps = 60;
var fpsInterval, startTime, now, then, elapsed;

var firstFishMode = false;
var firstFishWin = false;
var stoppedEnemies = 0;
var firstFishTries = 0;

/* GLOBALS */


function clone(obj) {
    if (null === obj || typeof obj != "object") return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function playSound(sound) {
	if(soundEnabled) {
	    sound.pause();
		sound.currentTime = 0;
	    sound.play();
	}
}
function goFullScreen(element) {
    if(canvas.requestFullScreen)
        canvas.requestFullScreen();
    else if(canvas.webkitRequestFullScreen)
        canvas.webkitRequestFullScreen();
    else if(canvas.mozRequestFullScreen)
        canvas.mozRequestFullScreen();
}

function refreshCanvasSize() {
	//	set screen size to window size
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
}

// if player and enemy intersect
function intersectPlayer(e) {
	return !(e.x > player.x + player.sizeX ||
			 e.x + e.sizeX < player.x ||
			 e.y > player.y + player.sizeY ||
			 e.y + e.sizeY < player.y);
}